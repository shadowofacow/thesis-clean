import tensorflow as tf
import os
import numpy as np

NUM_LETTERS = 26
CONV1_FILTERS = 256
CONV1_FILTER_SIZE = 25 
POOL1_SIZE = 5
CONV2_FILTERS = 128
CONV2_FILTER_SIZE = 10
POOL2_SIZE = 2
DENSE_UNITS = 256

LEARNING_RATE = 0.001

def cnn_model_fn(features, labels, mode):
    """Model function for CNN."""
    # Input Layer (out: 100 x 1 x 3)
    input_layer = tf.reshape(features["x"], [-1, 100, 1, 6]) # changed 3 to 6 for gyro

    # Visualize input
##    input_image = tf.transpose(input_layer, [0, 3, 1, 2])
##    tf.summary.image("input", input_image, max_outputs=10)

    # Convolutional Layer #1 (out: 100 x 1 x 32)
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=CONV1_FILTERS,
        kernel_size=[CONV1_FILTER_SIZE, 1],
        padding="same",
        activation=tf.nn.relu,
        name="conv1")

    # Visualize output of conv1
##    conv1_t = tf.transpose(conv1[0], [2, 0, 1])
##    for f in range(32):
##        tf.summary.histogram("output" + str(f), conv1_t[f])

    # Visualize filters
##    with tf.variable_scope("conv1") as scope:
##        tf.get_variable_scope().reuse_variables()
##        weights = tf.get_variable("kernel") # 100 x 1 x 3 x 32
##        #weights_t = tf.transpose(weights, [3, 1, 0, 2]) # colours
##        weights_t = tf.transpose(weights, [3, 2, 0, 1]) # grayscale
##        tf.summary.image("conv1/filters", weights_t, max_outputs=32)

    # Pooling Layer #1 (out: 20 x 1 x 32)
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[POOL1_SIZE, 1],
                                    strides=POOL1_SIZE)

    # Convolutional Layer #2 and Pooling Layer #2 (out: 10 x 1 x 64)
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=CONV2_FILTERS,
        kernel_size=[CONV2_FILTER_SIZE, 1],
        padding="same",
        activation=tf.nn.relu,
        name="conv2")

    # Visualize filters
##    with tf.variable_scope("conv2") as scope:
##        tf.get_variable_scope().reuse_variables()
##        weights = tf.get_variable("kernel") # 20 x 1 x 32 x 64
##        #weights_t = tf.transpose(weights, [3, 1, 0, 2]) # colours
##        weights_t = tf.transpose(weights, [3, 2, 0, 1]) # grayscale
##        tf.summary.image("conv2/filters", weights_t, max_outputs=64)
        
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[POOL2_SIZE, 1],
                                    strides=POOL2_SIZE)

    # Dense Layer (out: 640)
    
    pool2_flat = tf.reshape(pool2, [-1, int(CONV2_FILTERS * 100 / POOL1_SIZE / POOL2_SIZE)])
    dense = tf.layers.dense(inputs=pool2_flat, units=DENSE_UNITS, activation=tf.nn.relu)
    #dropout = tf.layers.dropout(
    #    inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

    # Logits Layer
    logits = tf.layers.dense(inputs=dense, units=NUM_LETTERS, name="logits")
    # Visualize weights
##    with tf.variable_scope("logits") as scope:
##        tf.get_variable_scope().reuse_variables()
##        weights = tf.get_variable("kernel") # 100 x 1 x 3 x 32
##        print(weights.get_shape())

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=logits, axis=1),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=NUM_LETTERS)
    loss = tf.losses.softmax_cross_entropy(
        onehot_labels=onehot_labels, logits=logits)
    tf.summary.scalar("loss", loss)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=LEARNING_RATE)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

