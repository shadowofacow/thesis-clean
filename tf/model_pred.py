import numpy as np
import tensorflow as tf
from model_fn import cnn_model_fn

tf.logging.set_verbosity(tf.logging.INFO)

MODEL_DIR = "tmp/sweep_model"

def main(argv=None):
    # Load eval data
    np_valid_data = np.load("npydata/all_eval_data.npy")
    np_valid_labels = np.load("npydata/all_eval_labels.npy")
        
    # Create the Estimator
    abc_classifier = tf.estimator.Estimator(
        model_fn=cnn_model_fn, model_dir=MODEL_DIR)

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": np_valid_data},
        y=np_valid_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = abc_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results["accuracy"])

    # Use Predict mode to print predictions
    pred_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": np_valid_data},
        y=np_valid_labels,
        num_epochs=1,
        shuffle=False)
    pred_results = abc_classifier.predict(input_fn=pred_input_fn)
    with open("predict_results.txt", 'w') as f:
        i = 0
        for pred in pred_results:
            arr = pred["probabilities"]
            pred = np.argmax(arr)
            f.write("" + chr(97+np_valid_labels[i]) + ":" + chr(97+pred) + "\n")
            print("" + chr(97+np_valid_labels[i]) + ":" + chr(97+pred))
            i += 1

if __name__ == "__main__":
    tf.app.run()
    
