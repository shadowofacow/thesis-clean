import numpy as np

dirName = input("Which data? ")

all_data = np.load("npydata/" + dirName + "_data_norm.npy")
all_labels = np.load("npydata/" + dirName + "_labels.npy")

# 4 in train, 1 in eval
NUM_LETTERS = 26 # self-explanatory

count = [0 for i in range(26)]

train_data = []
train_labels = []
eval_data = []
eval_labels = []

for i in range(len(all_labels)):
    if count[all_labels[i]] % 5 == 4:
        data = eval_data
        labels = eval_labels
    else:
        data = train_data
        labels = train_labels
    count[all_labels[i]] += 1
    data.append(all_data[i])
    labels.append(all_labels[i])

np_train_data = np.asarray(train_data, dtype = np.float32)
np_train_labels = np.asarray(train_labels, dtype = np.int32)
np_eval_data = np.asarray(eval_data, dtype = np.float32)
np_eval_labels = np.asarray(eval_labels, dtype = np.int32)

np.save("npydata/" + dirName + "_train_data.npy", np_train_data)
np.save("npydata/" + dirName + "_train_labels.npy", np_train_labels)
np.save("npydata/" + dirName + "_eval_data.npy", np_eval_data)
np.save("npydata/" + dirName + "_eval_labels.npy", np_eval_labels)
