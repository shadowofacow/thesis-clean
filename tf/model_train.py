import numpy as np
import tensorflow as tf
from model_fn import cnn_model_fn

MODEL_DIR = "tmp/sweep_model"

tf.logging.set_verbosity(tf.logging.INFO)

# arguments:
def main(argv=None):
    # Load training data
    np_train_data = np.load("npydata/all_train_data.npy")
    np_train_labels = np.load("npydata/all_train_labels.npy")

#    config = tf.ConfigProto()
#    config.gpu_options.per_process_gpu_memory_fraction = 0.0
#    config.gpu_options.allow_growth = True
#    run_config = tf.estimator.RunConfig(session_config = config)

    # Create the Estimator
    abc_classifier = tf.estimator.Estimator(
        model_fn=cnn_model_fn, model_dir=MODEL_DIR) #, config=run_config)

    # Set up logging for predictions
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": np_train_data},
        y=np_train_labels,
        batch_size=52,
        num_epochs=None,
        shuffle=True)
   
    abc_classifier.train(
        input_fn=train_input_fn,
        steps=10000,
        hooks=[logging_hook])

if __name__ == "__main__":
    tf.app.run()
    
