import tensorflow as tf
import numpy as np

dirName = input("Which data? ")

all_data = np.load("npydata/" + dirName + "_data_norm.npy")

# convert to [batch_size, 6, 100, 1]
input_image = tf.expand_dims(tf.transpose(all_data, [0, 2, 1]), -1)
# concatenate same letter
input_image = tf.reshape(input_image, [26, -1, 100, 1])
tf.summary.image("input", input_image, max_outputs=26)

sess = tf.Session()
writer = tf.summary.FileWriter("tmp/display_inputs")

merged = tf.summary.merge_all()
summ = sess.run(merged)
writer.add_summary(summ)

writer.close()
sess.close()
