# converts data files from csv to npy array
# inputs:
#   dirs
# outputs:
#   [dir]_data.npy
#   [dir]_labels.npy

import os
import numpy as np

dirName = input("Enter a subdirectory of 'sensordata': ")

sensor_data_path = "sensordata/" + dirName
dataDir = os.fsencode(sensor_data_path)

data = []
labels = []

line_count = 0 # to make sure data is valid

for file in os.listdir(dataDir):
    filename = os.fsdecode(file)

    index = ord(filename[0]) - 97
    labels.append(index)
    
    curFile = os.path.join(sensor_data_path, filename)
    dataArr = []
    with open(curFile) as f:
        line = f.readline().strip()
        while line:
            arr = line.split(",")
            dataArr.append(arr[1:7])
            line_count += 1
            line = f.readline().strip()
        if line_count is not 100:
            print("------------ ERROR ------------")
        line_count = 0
    data.append(dataArr)

np_data = np.asarray(data, dtype = np.float32)
np_labels = np.asarray(labels, dtype = np.int32)
np.save("npydata/" + dirName + "_data.npy", np_data)
np.save("npydata/" + dirName + "_labels.npy", np_labels)
