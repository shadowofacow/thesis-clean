import numpy as np

data = []
labels = []

while True:
    line = input("Which data? ('q' to quit) ")
    if line.strip() == 'q':
        break
    data.append(np.load("npydata/" + line + "_data.npy"))
    labels.append(np.load("npydata/" + line + "_labels.npy"))

# concatenate all data
combined_data = np.concatenate(data, axis=0)
combined_labels = np.concatenate(labels, axis=0)
    
# save data
newName = input("Name of combined data: ")
np.save("npydata/" + newName + "_data.npy", combined_data)
np.save("npydata/" + newName + "_labels.npy", combined_labels)
