import numpy as np

dirName = input("Which data? ")

all_data = np.load("npydata/" + dirName + "_data.npy")

# normalize data (subtract mean)
for sample in all_data:
    mean = np.mean(sample, axis=0)
    std = np.std(sample, axis=0)
    for row in sample:
        np.subtract(row, mean, out=row)
        np.divide(row, std, out=row)

# save data
np.save("npydata/" + dirName + "_data_norm.npy", all_data)
